package br.ucsal.ads20211.testequalidade.atividade02;

import java.io.PrintStream;
import java.util.Scanner;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Questao01.class })
public class Questao01Test {

	@Test
	public void testarObterNumeroFaixa1() throws Exception {
		// Entrada: -9, 1050, -8, 15
		Integer nEsperado = 15;

		Scanner scannerMock = Mockito.mock(Scanner.class);
		Mockito.when(scannerMock.nextInt()).thenReturn(-9).thenReturn(1050).thenReturn(-8).thenReturn(15);
		// Whitebox.setInternalState(Questao01.class, "scanner", scannerMock);
		PowerMockito.whenNew(Scanner.class).withArguments(System.in).thenReturn(scannerMock);

		PrintStream printStreamMock = Mockito.mock(PrintStream.class);
		System.setOut(printStreamMock);

		Integer nAtual = Questao01.obterNumeroFaixa();

		Assert.assertEquals(nEsperado, nAtual);
		Mockito.verify(printStreamMock, Mockito.times(3)).println("Número fora da faixa.");
	}

	@Test
	public void testarExibirPrimoNaoPrimo() {
		Integer n = 5;
		Boolean isPrimo = true;

		String mensagemEsperada = "O número 5 é primo";

		PrintStream printStreamMock = Mockito.mock(PrintStream.class);
		System.setOut(printStreamMock);

		Questao01.exibirPrimoNaoPrimo(n, isPrimo);

		Mockito.verify(printStreamMock).println(mensagemEsperada);
	}

}
