package br.ucsal._20202.testequalidade.aula03;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.interfaces.List;

// NÃO FAÇA ASSIM!
// DEVEMOS USAR FRAMEWORKS DE TESTE PARA TESTAR NOSSAS APLICAÇÕES.
public class LinkedListTestSemUsarJUnit {

	public static void main(String[] args) throws InvalidElementException {
		testarAdd1Elemento();
		testarAdd3Elementos();
	}

	private static void testarAdd1Elemento() throws InvalidElementException {
		List<String> nomes = new LinkedList<>();
		nomes.add("claudio");
		if (nomes.get(0).equals("claudio")) {
			System.out.println("testarAdd1Elemento sucesso.");
		} else {
			System.out.println("testarAdd1Elemento falha.");
		}
	}

	private static void testarAdd3Elementos() throws InvalidElementException {
		List<String> nomes = new LinkedList<>();
		nomes.add("claudio");
		nomes.add("antonio");
		nomes.add("neiva");
		if ("claudio".equals(nomes.get(0)) && "antonio".equals(nomes.get(1)) && "neiva".equals(nomes.get(2))) {
			System.out.println("testarAdd3Elementos sucesso.");
		} else {
			System.out.println("testarAdd3Elementos falha.");
		}
	}

}
