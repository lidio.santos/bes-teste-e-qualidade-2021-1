package br.ucsal._20202.testequalidade.aula03.util;

import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class CalculoUtilTest {

	@ParameterizedTest(name = "{index} calcularFatorial({0})")
	@MethodSource("fornecerDadosTest")
	void testarFatorial(Integer n, Long fatorialEsperado) {
		// Execução do método que está sendo testado e obtenção da saída atual
		Long fatorialAtual = CalculoUtil.calcularFatorial(n);

		// Comprar a saída esperada com a saída atual
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

	private static Stream<Arguments> fornecerDadosTest() {
	    return Stream.of(
	      Arguments.of(0, 1L),
	      Arguments.of(1, 1L),
	      Arguments.of(5, 120L)
	    );
	    
		// List<Arguments> argumentos = new ArrayList<>();
		// argumentos.add(Arguments.of(0, 1L));
		// argumentos.add(Arguments.of(1, 1L));
		// argumentos.add(Arguments.of(5, 120L));
		// return argumentos.stream();
	}

}
