package br.ucsal.bes20211.testequalidade.aula07.builder;

import java.time.LocalDate;

import br.ucsal.bes20211.testequalidade.aula07.domain.Aluno;
import br.ucsal.bes20211.testequalidade.aula07.domain.SituacaoAlunoEnum;

public class AlunoBuilder {

	private static final Integer DEFAULT_MATRICULA = 1;
	private static final String DEFAULT_NOME = "Claudio Neiva";
	private static final LocalDate DEFAULT_DATA_NASCIMENTO = null;
	private static final String DEFAULT_EMAIL = "claudio@ucsal.br";
	private static final SituacaoAlunoEnum DEFAULT_SITUACAO = null;

	private Integer matricula = DEFAULT_MATRICULA;
	private String nome = DEFAULT_NOME;
	private LocalDate dataNascimento = DEFAULT_DATA_NASCIMENTO;
	private String email = DEFAULT_EMAIL;
	private SituacaoAlunoEnum situacao = DEFAULT_SITUACAO;

	private AlunoBuilder() {
	}

	public static AlunoBuilder umAluno() {
		return new AlunoBuilder();
	}

	public static AlunoBuilder umAlunoAtivo() {
		return new AlunoBuilder().ativo();
	}

	public static AlunoBuilder umAlunoTrancado() {
		return new AlunoBuilder().trancado();
	}

	public AlunoBuilder comMatricula(Integer matricula) {
		this.matricula = matricula;
		return this;
	}

	public AlunoBuilder comNome(String nome) {
		this.nome = nome;
		return this;
	}

	public AlunoBuilder nascidoEm(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
		return this;
	}

	public AlunoBuilder nascidoEm(int dia, int mes, int ano) {
		this.dataNascimento = LocalDate.of(ano, mes, dia);
		return this;
	}

	public AlunoBuilder comEmail(String email) {
		this.email = email;
		return this;
	}

	public AlunoBuilder naSituacao(SituacaoAlunoEnum situacao) {
		this.situacao = situacao;
		return this;
	}

	public AlunoBuilder ativo() {
		this.situacao = SituacaoAlunoEnum.ATIVO;
		return this;
	}

	public AlunoBuilder formado() {
		this.situacao = SituacaoAlunoEnum.FORMADO;
		return this;
	}

	public AlunoBuilder trancado() {
		this.situacao = SituacaoAlunoEnum.TRANCADO;
		return this;
	}

	public AlunoBuilder mas() {
		return new AlunoBuilder().comMatricula(matricula).comNome(nome).comEmail(email).nascidoEm(dataNascimento)
				.naSituacao(situacao);
	}

	public Aluno build() {
		Aluno aluno = new Aluno(matricula, nome);
		aluno.setEmail(email);
		aluno.setDataNascimento(dataNascimento);
		aluno.setSituacao(situacao);
		return aluno;
	}

}
