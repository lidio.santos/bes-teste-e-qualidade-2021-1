package br.ucsal.bes20211.testequalidade.aula07.domain;

import java.time.LocalDate;

public class Aluno {

	private Integer matricula;

	private String nome;

	private LocalDate dataNascimento;

	private String email;

	private SituacaoAlunoEnum situacao;

	public Aluno(Integer matricula, String nome) {
		super();
		this.matricula = matricula;
		this.nome = nome;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public SituacaoAlunoEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoAlunoEnum situacao) {
		this.situacao = situacao;
	}

	@Override
	public String toString() {
		return "Aluno [matricula=" + matricula + ", nome=" + nome + ", dataNascimento=" + dataNascimento + ", email="
				+ email + ", situacao=" + situacao + "]";
	}

}
