package br.ucsal.bes20211.testequalidade.aula07.domain;

public enum SituacaoAlunoEnum {
	
	ATIVO, TRANCADO, FORMADO;

}
