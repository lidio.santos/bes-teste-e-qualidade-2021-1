package br.ucsal.bes20192.testequalidade.escola.business;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

public class AlunoBOTest {

	private AlunoBO alunoBO;
	private AlunoDAO alunoDAOMock;
	private DateHelper dateHelperMock;
	private Integer anoAtual = 2021;

	@BeforeEach
	void setup() {
		alunoDAOMock = Mockito.mock(AlunoDAO.class);
		dateHelperMock = Mockito.mock(DateHelper.class);
		alunoBO = new AlunoBO(alunoDAOMock, dateHelperMock);

		// Configurar o dateHelperMock para que retorne 2021 na chamada ao
		// obterAnoAtual()
		Mockito.when(dateHelperMock.obterAnoAtual()).thenReturn(anoAtual);
	}

	@Test
	void testarCalcularIdade() {
		Integer matricula = 1;
		Aluno aluno1 = AlunoBuilder.umAluno().comMatricula(matricula).nascidoEm(2000).build();

		Integer idadeEsperada = 21;

		// Configurar o alunoDAOMock para retornar o aluno1, quando for chamado o
		// encontrarPorMatricula(matricula)
		Mockito.when(alunoDAOMock.encontrarPorMatricula(matricula)).thenReturn(aluno1);

		Integer idadeAtual = alunoBO.calcularIdade(matricula);

		Assertions.assertEquals(idadeEsperada, idadeAtual);
	}

	@Test
	void testarCalcularIdade2() {
		Integer matricula = 1;
		Aluno aluno1 = AlunoBuilder.umAluno().comMatricula(matricula).nascidoEm(1996).build();

		Integer idadeEsperada = 25;

		Mockito.when(alunoDAOMock.encontrarPorMatricula(matricula)).thenReturn(aluno1);

		Integer idadeAtual = alunoBO.calcularIdade(matricula);

		Assertions.assertEquals(idadeEsperada, idadeAtual);
	}

	@Test
	void testarAtualizarAlunoAtivo() {
		Aluno alunoEsperado = AlunoBuilder.umAlunoAtivo().build();

		alunoBO.atualizar(alunoEsperado);

		// Verificar se foi chamado para alunoDAOMock o método salvar,
		// passando como parâmetro o objeto alunoEsperado.
		Mockito.verify(alunoDAOMock).salvar(alunoEsperado);
	}

}
