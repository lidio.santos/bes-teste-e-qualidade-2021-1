package br.ucsal.bes20192.testequalidade.escola.dubles;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;

public class AlunoDAOFakeDuble extends AlunoDAO {

	private static final List<Aluno> alunos = new ArrayList<>();

	@Override
	public void salvar(Aluno aluno) {
		alunos.add(aluno);
	}

	@Override
	public void excluirTodos() {
		alunos.clear();
	}

	@Override
	public Aluno encontrarPorMatricula(Integer matricula) {
		for (Aluno aluno : alunos) {
			if (matricula.equals(aluno.getMatricula())) {
				return aluno;
			}
		}
		return null;
	}

}
