package br.ucsal.bes20192.testequalidade.escola.dubles;

import java.util.HashMap;
import java.util.Map;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;

public class AlunoDAOMockDuble extends AlunoDAO {

	// Map<o aluno que passado para o método salvar, qtd de chamadas feitas>
	private Map<Aluno, Integer> qtdChamadasSalvarPorAluno = new HashMap<>();

	private Integer anoNascimento;

	public void definirAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	@Override
	public void salvar(Aluno aluno) {
		if (!qtdChamadasSalvarPorAluno.containsKey(aluno)) {
			qtdChamadasSalvarPorAluno.put(aluno, 0);
		}
		qtdChamadasSalvarPorAluno.put(aluno, qtdChamadasSalvarPorAluno.get(aluno) + 1);
	}

	@Override
	public void excluirTodos() {
	}
	
	@Override
	public Aluno encontrarPorMatricula(Integer matricula) {
		Aluno aluno = new Aluno();
		aluno.setAnoNascimento(anoNascimento);
		return aluno;
	}

	public void verificarChamadasSalvar(Aluno aluno, Integer qtdEsperada) {
		if (!qtdChamadasSalvarPorAluno.containsKey(aluno)) {
			throw new RuntimeException("Nenhuma chamada para o método salvar passando o aluno " + aluno);
		}
		if (!qtdChamadasSalvarPorAluno.get(aluno).equals(qtdEsperada)) {
			throw new RuntimeException("Chamadas esperadas " + qtdEsperada + " diferente da atual "
					+ qtdChamadasSalvarPorAluno.get(aluno));
		}
	}

}
