package br.ucsal.bes20192.testequalidade.escola.testescomdublemanual;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20192.testequalidade.escola.business.AlunoBO;
import br.ucsal.bes20192.testequalidade.escola.dubles.AlunoDAOStubDuble;
import br.ucsal.bes20192.testequalidade.escola.dubles.DateHelperStubDuble;

public class AlunoBO2Test {

	private AlunoBO alunoBO;
	private AlunoDAOStubDuble alunoDAOStub;
	private DateHelperStubDuble dateHelperStub;

	@BeforeEach
	void setup() {
		// Pra o teste do AlunoBO.calcularIdade seja unitário, o AlunoDAO e o
		// DateHelper deve usar dublês!
		alunoDAOStub = new AlunoDAOStubDuble();	
		dateHelperStub = new DateHelperStubDuble();
		alunoBO = new AlunoBO(alunoDAOStub, dateHelperStub);
	}

	@Test
	void testarCalcularIdade() {
		Integer anoAtual = 2021;
		Integer anoNascimentoAtual = 2000;
		Integer idadeEsperada = 21;

		dateHelperStub.definirAnoAtual(anoAtual);
		alunoDAOStub.definirAnoNascimento(anoNascimentoAtual);
		
		// O método calcularIdade é chamado QUERY.
		Integer idadeAtual = alunoBO.calcularIdade(10);

		Assertions.assertEquals(idadeEsperada, idadeAtual);
	}

}
